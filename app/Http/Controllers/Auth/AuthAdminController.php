<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthAdminController extends Controller
{
    public function login(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $admin = Admin::where('email', $request->email)->first();

        if (!$admin || !Hash::check($request->password, $admin->password)) {
            return response([
                'success' => false,
                'message' => "Credentials Tidak Cocok!"
            ], 404);
        }

        $token = $admin->createToken('ApiToken')->plainTextToken;

        $response = [
            'success' => true,
            'user' => $admin,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        $response = [
            'success' => true,
            'message' => 'Logout Success'
        ];
        return response($response, 200);
    }
}
