require('./bootstrap');
import { createApp } from 'vue';
import router from "./routes.js";
import Index from "./components/IndexComponent.vue";
import axios from "axios";

axios.defaults.withCredentials = true;

const app = createApp({});

app.use(router);

app.component('index', Index).default;

app.mount("#app");