import { createWebHistory, createRouter } from "vue-router";
import RouteHome from "./router/home";
import RouteAdmin from "./router/admin";

const routes = [];

routes.push.apply(routes, RouteHome);

routes.push.apply(routes, RouteAdmin);

console.log(routes);

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    document.title = to.meta.title;
    next();
})

export default router;