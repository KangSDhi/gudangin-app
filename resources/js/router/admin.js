import DashboardAdmin from "../components/dashboard/IndexComponent";
import ConsoleAdmin from "../components/dashboard/ConsoleComponent";

const routes = [
    {
        path: '/dashboard',
        component: DashboardAdmin,
        name: 'Dashboard',
        meta: {
            title: "Dashboard Admin"
        }
    },
    {
        path: '/console',
        component: ConsoleAdmin,
        name: 'Console',
        meta: {
            title: "Console Admin"
        }
    }
];

export default routes;