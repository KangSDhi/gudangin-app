import Home from "../components/home/HomeComponent.vue";
import About from "../components/home/AboutComponent.vue";
import Login from "../components/auth/LoginComponent.vue";

const routes = [
    {
        path: '/',
        component: Home,
        name: 'Home',
        meta: {
            title: "Halaman Home"
        }
    },
    {
        path: '/about',
        component: About,
        name: 'About',
        meta: {
            title: "Halaman About"
        }
    },
    {
        path: '/login',
        component: Login,
        name: 'Login',
        meta: {
            title: "Halaman Login"
        }
    }
];

export default routes;